@extends('layouts.app')

@section('content')
{{ $errors }}
<div class="container-fluid">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">Dashboard</div>
                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif

                   <div class="row">
                        <div class="col-3">
                            <div class="nav flex-column nav-pills" id="v-pills-tab" role="tablist" aria-orientation="vertical">
                            <a class="nav-link active" id="v-pills-home-tab" data-toggle="pill" href="#v-pills-home" role="tab" aria-controls="v-pills-home" aria-selected="true">WorkSpace</a>
                            <a class="nav-link" id="v-pills-profile-tab" data-toggle="pill" href="#v-pills-profile" role="tab" aria-controls="v-pills-profile" aria-selected="false">Profile</a>
                            </div>
                        </div>
                        <div class="col-9">
                            <div class="tab-content" id="v-pills-tabContent">
                            <div class="tab-pane fade show active" id="v-pills-home" role="tabpanel" aria-labelledby="v-pills-home-tab">
                               <nav>
                                <div class="nav nav-tabs" id="nav-tab" role="tablist">
                                    <a class="nav-item nav-link active" id="nav-home-tab" data-toggle="tab" href="#nav-home" role="tab" aria-controls="nav-home" aria-selected="true">Agenda</a>
                                    <a class="nav-item nav-link" id="nav-profile-tab" data-toggle="tab" href="#nav-profile" role="tab" aria-controls="nav-profile" aria-selected="false">Formuler Requête</a>
                                </div>
                                </nav>
                                <div class="tab-content" id="nav-tabContent">
                                <div class="tab-pane fade show active" id="nav-home" role="tabpanel" aria-labelledby="nav-home-tab">
                                    Agenda
                                
                                </div>
                                <div class="tab-pane fade" id="nav-profile" role="tabpanel" aria-labelledby="nav-profile-tab">
                                    <form class="m-3">
                                       
                                        <div class="form-group">
                                            <label for="exampleInputPassword1">Superviseur</label>
                                            <select class="form-control" >
                                                <option>1</option>
                                            </select>
                                        </div>
                                        <div class="form-group">
                                            <label for="exampleInputPassword1">Descrption</label>
                                               <textarea class="form-control" id="exampleFormControlTextarea1" rows="3"></textarea>
                                        </div>
                                        
                                        <button type="submit" class="btn btn-primary">Requete</button>
                                    </form>
                                </div>
                                </div>
                            </div>
                            <div class="tab-pane fade" id="v-pills-profile" role="tabpanel" aria-labelledby="v-pills-profile-tab">
                                  <form action="{{ route('profile_update')}}" method="post" id="update">

                                            

                                            <div class="form-group">
                                                <label >Nom</label>
                                                <input type="nom" class="form-control" placeholder="Nom" name="nom" value="{{ $user->nom}}">
                                            </div>

                                            <div class="form-group">
                                                <label >Prenom</label>
                                                <input type="prenom" class="form-control" placeholder="Prénom" name="prenom" value="{{ $user->prenom}}">
                                            </div>

                                            <div class="form-group">
                                                <label >Addresse Email</label>
                                                <input type="email" class="form-control" placeholder=" Email" name='email' value="{{ $user->email}}"

                                                >
                                            </div>

                                            <div class="form-group">
                                                <label >Tel</label>
                                                <input type="tel" class="form-control" placeholder="Téléphone" name="tel" value="{{ $user->tel}}">
                                            </div>
                                              <div class="form-group">
                                                <label for="exampleFormControlSelect1">Role</label>
                                                <input type="tel" class="form-control"  name="role" value="{{ $user->role}}">
                                            </div>
                                            <div class="form-group">
                                                <label >Username</label>
                                                <input type="text" class="form-control" placeholder="Nom d'utilisateur" name="name" value="{{ $user->name}}">
                                            </div>


                                            <button type="submit" class="btn btn-primary">Envoyer</button>
                                        </form>
                            </div>
                            </div>
                        </div>
                        </div>
                </div>
            </div>
        </div>
    </div>
</div>


@endsection
