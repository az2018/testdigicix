@extends('layouts.app')

@section('content')
<div class="container-fluid">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">Dashboard</div>
                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif

                   <div class="row">
                        <div class="col-3">
                            <div class="nav flex-column nav-pills" id="v-pills-tab" role="tablist" aria-orientation="vertical">
                            <a class="nav-link active" id="v-pills-home-tab" data-toggle="pill" href="#v-pills-home" role="tab" aria-controls="v-pills-home" aria-selected="true">WorkSpace</a>
                            <a class="nav-link" id="v-pills-profile-tab" data-toggle="pill" href="#v-pills-profile" role="tab" aria-controls="v-pills-profile" aria-selected="false">Profile</a>
                            </div>
                        </div>
                        <div class="col-9">
                            <div class="tab-content" id="v-pills-tabContent">
                            <div class="tab-pane fade show active" id="v-pills-home" role="tabpanel" aria-labelledby="v-pills-home-tab">
                                <p>
                                <button class="btn btn-primary" type="button" data-toggle="collapse" data-target="#collapseExample" aria-expanded="false" aria-controls="collapseExample">
                                    Créer Utilisateur
                                </button>
                                
                                </p>
                                <div class="collapse" id="collapseExample">
                                    <div class="card card-body">
                                         <form action="{{ route('create_user')}}" method="post" id="create">
                                            <div class="form-group">
                                                <label >Nom</label>
                                                <input type="nom" class="form-control" placeholder="Adresse" name="nom">
                                            </div>

                                            <div class="form-group">
                                                <label >Prenom</label>
                                                <input type="prenom" class="form-control" placeholder="Prénom" name="prenom">
                                            </div>

                                            <div class="form-group">
                                                <label >Addresse Email</label>
                                                <input type="email" class="form-control" placeholder=" Email" name='email'>
                                            </div>

                                            <div class="form-group">
                                                <label >Tel</label>
                                                <input type="tel" class="form-control" placeholder="Téléphone" name="tel">
                                            </div>
                                              <div class="form-group">
                                                <label for="exampleFormControlSelect1">Role</label>
                                                <select class="form-control" name="role">
                                                    <option value="admin">admin</option>
                                                    <option value="etudiant">etudiant</option>
                                                    <option value="admin">superviseur</option>
                                                </select>
                                            </div>
                                            <div class="form-group">
                                                <label >Password</label>
                                                <input type="password" class="form-control" placeholder="Le mot de passe" name="password">
                                            </div>

                                            <div class="form-group">
                                                <label >Confirmer le mot de passe</label>
                                                <input type="password" class="form-control" placeholder="Le mot de passe" name="passconf">
                                            </div>

                                            <button type="submit" class="btn btn-primary">Envoyer</button>
                                        </form>
                                    </div>
                                </div>
                                <div class="container-fluid m-3 pr-3">
                                    <div class="row">
                                        <div class="col-sm-12">
                                            <table class="table table-bordered">
                                                <thead>
                                                    <tr>
                                                    <th scope="col">#</th>
                                                    <th scope="col">Nom</th>
                                                    <th scope="col">Prenom</th>
                                                    <th scope="col">Adresse Email</th>
                                                    <th scope="col">Role</th>
                                                    <th scope="col">Telphone</th>
                                                    <th scope="col">Action</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    @foreach ($users as $user)
                                                        <tr>
                                                            <td> {{ $user->id }}</td>
                                                            <td> {{ $user->nom }}</td>
                                                            <td> {{ $user->prenom }}</td>
                                                            <td> {{ $user->email }}</td>
                                                            <td> {{ $user->role }}</td>
                                                            <td> 
                                                                @if ($user->role=="etudiant" && $user->limit < 3)
                                                                    <button class="btn btn-primary" data-toggle="modal" data-target="#exampleModal" >Affecter a un Prof</button>
                                                                @endif
                                                            </td>
                                                             <td> {{ $user->tel }}</td>
                                                        </tr>
                                                    @endforeach
                                                </tbody>
                                            </table>

                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="tab-pane fade" id="v-pills-profile" role="tabpanel" aria-labelledby="v-pills-profile-tab">
                                  <form action="{{ route('profile_update')}}" method="post" id="update">


                                            <div class="form-group">
                                                <label >Nom</label>
                                                <input type="nom" class="form-control" placeholder="Nom" name="nom" value="{{ $user->nom}}">
                                            </div>

                                            <div class="form-group">
                                                <label >Prenom</label>
                                                <input type="prenom" class="form-control" placeholder="Prénom" name="prenom" value="{{ $user->prenom}}">
                                            </div>

                                            <div class="form-group">
                                                <label >Addresse Email</label>
                                                <input type="email" class="form-control" placeholder=" Email" name='email' value="{{ $user->email}}"

                                                >
                                            </div>

                                            <div class="form-group">
                                                <label >Tel</label>
                                                <input type="tel" class="form-control" placeholder="Téléphone" name="tel" value="{{ $user->tel}}">
                                            </div>
                                              <div class="form-group">
                                                <label for="exampleFormControlSelect1">Role</label>
                                                <input type="tel" class="form-control"  name="role" value="{{ $user->role}}" disabled>
                                            </div>
                                            <div class="form-group">
                                                <label >Username</label>
                                                <input type="text" class="form-control" placeholder="Nom d'utilisateur" name="name" value="{{ $user->name}}">
                                            </div>


                                            <button type="submit" class="btn btn-primary">Envoyer</button>
                                        </form>
                            </div>
                            </div>
                        </div>
                        </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Modal title</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <div class="form-group">
            <label for="exampleFormControlSelect1">Superviseur</label>
            <select class="form-control" id="exampleFormControlSelect1">
           
                @foreach ($superviseurs as $superviseur )
                    <option value='{{ $superviseur->nom }}'>{{ $superviseur->nom }}</option>
                @endforeach
            </select>
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Valider</button>

      </div>
    </div>
  </div>
</div>

@endsection
