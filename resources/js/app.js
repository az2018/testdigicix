/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');

window.Vue = require('vue');

/**
 * The following block of code may be used to automatically register your
 * Vue components. It will recursively scan this directory for the Vue
 * components and automatically register them with their "basename".
 *
 * Eg. ./components/ExampleComponent.vue -> <example-component></example-component>
 */

// const files = require.context('./', true, /\.vue$/i);
// files.keys().map(key => Vue.component(key.split('/').pop().split('.')[0], files(key).default));

Vue.component('example-component', require('./components/ExampleComponent.vue').default);

/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

const app = new Vue({
    el: '#app',
});

$(document).ready(function(){
    jQuery.fn.serializeObject = function(){
        var results = {},
            arr = this.serializeArray();
        for (var i = 0, len = arr.length; i < len; i++) {
          obj = arr[i];
          //Check if results have a property with given name
          if (results.hasOwnProperty(obj.name)) {
            //Check if given object is an array
            if (!results[obj.name].push) {
              results[obj.name] = [results[obj.name]];
            }
            results[obj.name].push(obj.value || '');
          } else {
            results[obj.name] = obj.value || '';
          }
        }
        return results;
    }
   $('#create').on('submit',function(e){
        var form=$(this);
        e.preventDefault();
        console.log();
        axios.post('/api/create/user',form.serializeObject()).then(({ data }) =>{
            console.log(data);
        })
   })
   $('#update').on('submit',function(e){
        var form=$(this);
        e.preventDefault();
        var data=form.serializeObject();
        data['id']=parseInt(document.head.querySelector('meta[name="user"]').content);
        console.log(data);
        axios.post('/api/update/user',data).then(({ data }) =>{
            console.log(data);
        })
   })
});