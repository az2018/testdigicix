<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use Hash;
class AdminController extends Controller
{
    public function update(Request $request){
        $request->validate(
            [
                "email"=>"required|unique:users",
               // "name"=>"required",
            ]
            );
            $request['password']=Hash::make($request['password']);
            // User::where('id',$request['id'])->update(['email' => $request['email']]);
            
       return User::where('id',$request['id'])->update(array_merge($request->except('id'),['name'=>' ']));
    }

    public function create(Request $request){
        
        $request->validate(
            [
                "email"=>"required|email|unique:users",
                "nom"=>"required",
                "prenom"=>"required",
                "tel"=>"required",
                "role"=>"required",
                "password"=>"required",
                "passconf"=>"required|same:password",
            ]
        );

       return  User::create(array_merge($request->all(),['name' => ' ']));
    }
}
